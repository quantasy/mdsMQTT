/*
 *   "SeMqWay"
 *
 *    SeMqWay(tm): A gateway to provide an MQTT-View for any micro-service (Service MQTT-Gateway).
 *
 *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *    Quellgasse 21, CH-2501 Biel, Switzerland
 *
 *    Licensed under Dual License consisting of:
 *    1. GNU Affero General Public License (AGPL) v3
 *    and
 *    2. Commercial license
 *
 *
 *    1. This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *     accordance with the commercial license agreement provided with the
 *     Software or, alternatively, in accordance with the terms contained in
 *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *
 *
 *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *
 *
 */
package ch.quantasy.mdsmqtt.communication.mqtt.v3;

import ch.quantasy.mdservice.message.AMessage;
import ch.quantasy.mdservice.message.annotations.Nullable;
import ch.quantasy.mdservice.message.annotations.Range;
import java.net.URI;

/**
 *
 * @author reto
 */
public class MQTTCommunicationIntent extends AMessage {

    public URI uri;
    public String clientID;
    public Boolean isCleanSession;
    public Boolean automaticReconnect;
    public Authentication authentication;
    public Testament testament;
    public Boolean connect;
    public Boolean isTLS;
    @Nullable
    @Range(from = 0, to = Integer.MAX_VALUE)
    public Integer keepAliveInterval;
    @Nullable
    @Range(from = 0, to = Integer.MAX_VALUE)
    public Integer connectionTimeout;

    public MQTTCommunicationIntent(String clientID, Boolean isCleanSession, Boolean automaticReconnect, Authentication authentication, Testament testament, Boolean connect, Integer keepAliveInterval, Integer connectionTimeout, URI uri, Boolean isTLS) {
        this.uri = uri;
        this.clientID = clientID;
        this.isCleanSession = isCleanSession;
        this.automaticReconnect = automaticReconnect;
        this.authentication = new Authentication(authentication);
        this.testament = new Testament(testament);
        this.connect = connect;
        this.keepAliveInterval = keepAliveInterval;
        this.connectionTimeout = connectionTimeout;
        this.isTLS = isTLS;
    }

    public MQTTCommunicationIntent(MQTTCommunicationIntent intent) {
        this.uri = intent.uri;
        this.clientID = intent.clientID;
        this.isCleanSession = intent.isCleanSession;
        this.automaticReconnect = intent.automaticReconnect;
        if (intent.authentication != null) {
            this.authentication = new Authentication(intent.authentication);
        }
        if (intent.testament != null) {
            this.testament = new Testament(intent.testament);
        }
        this.connect = intent.connect;
        this.keepAliveInterval = intent.keepAliveInterval;
        this.connectionTimeout = intent.connectionTimeout;
        this.isTLS = intent.isTLS;
    }

    public boolean isConnectable() {
        if (!isValid()) {
            return false;
        }

        if (this.uri == null) {
            return false;
        }
        if (this.clientID == null || this.clientID.length() < 1) {
            return false;
        }
        if (this.isCleanSession == null) {
            return false;
        }

        return true;
    }

    public MQTTCommunicationIntent() {
    }
}
