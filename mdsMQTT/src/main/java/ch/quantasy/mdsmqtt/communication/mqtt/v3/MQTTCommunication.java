/*
 * /*
 *  *   "SeMqWay"
 *  *
 *  *    SeMqWay(tm): A gateway to provide an MQTT-View for any micro-service (Service MQTT-Gateway).
 *  *
 *  *    Copyright (c) 2016 Bern University of Applied Sciences (BFH),
 *  *    Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *    Quellgasse 21, CH-2501 Biel, Switzerland
 *  *
 *  *    Licensed under Dual License consisting of:
 *  *    1. GNU Affero General Public License (AGPL) v3
 *  *    and
 *  *    2. Commercial license
 *  *
 *  *
 *  *    1. This program is free software: you can redistribute it and/or modify
 *  *     it under the terms of the GNU Affero General Public License as published by
 *  *     the Free Software Foundation, either version 3 of the License, or
 *  *     (at your option) any later version.
 *  *
 *  *     This program is distributed in the hope that it will be useful,
 *  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  *     GNU Affero General Public License for more details.
 *  *
 *  *     You should have received a copy of the GNU Affero General Public License
 *  *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  *
 *  *
 *  *    2. Licensees holding valid commercial licenses for TiMqWay may use this file in
 *  *     accordance with the commercial license agreement provided with the
 *  *     Software or, alternatively, in accordance with the terms contained in
 *  *     a written agreement between you and Bern University of Applied Sciences (BFH),
 *  *     Research Institute for Security in the Information Society (RISIS), Wireless Communications & Secure Internet of Things (WiCom & SIoT),
 *  *     Quellgasse 21, CH-2501 Biel, Switzerland.
 *  *
 *  *
 *  *     For further information contact <e-mail: reto.koenig@bfh.ch>
 *  *
 *  *
 */
package ch.quantasy.mdsmqtt.communication.mqtt.v3;

import com.hivemq.client.mqtt.MqttClient;
import com.hivemq.client.mqtt.MqttClientBuilder;
import com.hivemq.client.mqtt.datatypes.MqttQos;
import com.hivemq.client.mqtt.mqtt3.Mqtt3AsyncClient;
import com.hivemq.client.mqtt.mqtt3.message.auth.Mqtt3SimpleAuth;
import com.hivemq.client.mqtt.mqtt3.message.connect.Mqtt3Connect;
import com.hivemq.client.mqtt.mqtt3.message.connect.Mqtt3ConnectBuilder;
import com.hivemq.client.mqtt.mqtt3.message.publish.Mqtt3Publish;
import java.net.InetSocketAddress;

import java.util.Objects;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reto
 */
public class MQTTCommunication {

    private final MQTTCommunicationIntent intent;
    private final MqttClientBuilder mqttClientBuilder;
    private Mqtt3AsyncClient mqttClient;
    private Thread publisherThread;
    private final Publisher publisher;
    private boolean timeToQuit;
    private final MessageDispatcher dispatcher;
    private final Mqtt3ConnectBuilder connectBuilder;

    public MQTTCommunication(MessageDispatcher dispatcher) {
        this.dispatcher = dispatcher;
        mqttClientBuilder = MqttClient.builder();
        connectBuilder = Mqtt3Connect.builder();
        this.publisher = new Publisher();
        this.intent = new MQTTCommunicationIntent();
    }

    public synchronized void setIntent(MQTTCommunicationIntent intent) {
        //If communication is established (connected) nothing can be changed. It only accepts changes when not connected
        if (this.intent.connect != null && this.intent.connect) {
            return;
        }

        if (!intent.isValid()) {
            return;
        }

        if (intent.isCleanSession != null) {
            this.intent.isCleanSession = intent.isCleanSession;
        }
        if (intent.clientID != null) {
            mqttClientBuilder.identifier(intent.clientID);
            this.intent.clientID = intent.clientID;
        }

        if (intent.uri != null) {
            this.mqttClientBuilder.serverAddress(new InetSocketAddress(intent.uri.getHost(), intent.uri.getPort()
            ));
            this.intent.uri = intent.uri;
        }
        if (intent.testament != null) {
            this.intent.testament = new Testament(intent.testament);

        }
        if (intent.authentication != null) {
            this.intent.authentication = new Authentication(intent.authentication);
        }
        if (intent.automaticReconnect != null) {
            this.intent.automaticReconnect = intent.automaticReconnect;
            if (intent.automaticReconnect) {
                this.mqttClientBuilder.automaticReconnect().applyAutomaticReconnect();
            }
        }
        if (intent.isTLS != null) {
            this.intent.isTLS = intent.isTLS;
            if (intent.isTLS) {
                this.mqttClientBuilder.sslWithDefaultConfig();
            }
        }
        if (this.intent.isConnectable() && this.mqttClient == null) {
            build();
        }
        if (intent.connect != null && !intent.connect.equals(this.intent.connect)) {
            this.intent.connect = intent.connect;
            if (this.intent.connect && this.mqttClient != null) {
                connect();
            }
            if (!this.intent.connect) {
                disconnect();

            }

        }
    }

    public MQTTCommunicationIntent getIntent() {
        return new MQTTCommunicationIntent(intent);
    }

    private synchronized void build() {
        if (!intent.isConnectable()) {
            return;
        }
        if (isConnected()) {
            return;
        }

        if (mqttClient == null) {
            this.mqttClient = mqttClientBuilder.useMqttVersion3().buildAsync();
        }

        if (intent.isCleanSession != null) {
            connectBuilder.cleanSession(intent.isCleanSession);

        }
        if (intent.testament != null) {
            connectBuilder.willPublish(Mqtt3Publish.builder().topic(intent.testament.willTopic).payload(intent.testament.lastWillMessage).retain(intent.testament.isLastWillRetained).qos(MqttQos.fromCode(intent.testament.lastWillQoS)).build());
        }
        if (intent.authentication != null) {
            connectBuilder.simpleAuth(Mqtt3SimpleAuth.builder().username(intent.authentication.username).password(intent.authentication.password.getBytes()).build());
        }
        dispatcher.readyToConnect();

    }

    private void connect() {
        mqttClient.connect(connectBuilder.build()).whenComplete((connAck, throwable) -> {
            if (throwable != null) {
                dispatcher.connectionFailed();
            }
            if (publisherThread == null) {
                publisherThread = new Thread(publisher);
                //publisherThread.setDaemon(true);
                publisherThread.start();
            }
            dispatcher.connected();
        });

    }

    public void publishActualWill(byte[] actualWill) {
        this.mqttClient.publishWith()
                .topic(intent.testament.willTopic)
                .payload(actualWill)
                .qos(MqttQos.fromCode(intent.testament.lastWillQoS))
                .retain(intent.testament.isLastWillRetained)
                .send()
                .whenComplete((mqtt3Publish, throwable) -> {
                    if (throwable != null) {
                        // Handle failure to publish
                    } else {
                        // Handle successful publish, e.g. logging or incrementing a metric
                    }
                });
    }

    public void readyToPublish(MQTTMessageManager publisherCallback, String topic) {
        publisher.readyToPublish(publisherCallback, topic);
    }

    private synchronized void publish(String topic, MQTTMessage message) {
        if (topic == null || message == null) {
            return;
        }
        if (!isConnected()) {
            return;
        }
        this.mqttClient.publishWith()
                .topic(topic)
                .payload(message.message)
                .qos(MqttQos.fromCode(message.qos))
                .retain(message.isRetained)
                .send()
                .whenComplete((mqtt3Publish, throwable) -> {
                    if (throwable != null) {
                        // Handle failure to publish
                    } else {
                        // Handle successful publish, e.g. logging or incrementing a metric
                    }
                });
    }

    public void quit() {
        this.timeToQuit = true;
        this.publisherThread.interrupt();
        this.disconnect();

    }

    public synchronized void subscribe(String topic, int qualityOfService) {

        if (!isConnected()) {
            return;
        }
        mqttClient.subscribeWith()
                .topicFilter(topic).qos(MqttQos.fromCode(qualityOfService))
                .callback(publish -> {
                    dispatcher.messageArrived(publish);
                })
                .send()
                .whenComplete((subAck, throwable) -> {
                    if (throwable != null) {
                        System.out.println("THROW " + throwable);// Handle failure to subscribe
                    } else {
                        // Handle successful subscription, e.g. logging or incrementing a metric
                    }
                });
    }

    public synchronized void unsubscribe(String topic) {

        if (!isConnected()) {
            return;
        }
        mqttClient.unsubscribeWith().topicFilter(topic).send();

    }

    private synchronized void disconnect() {
        if (mqttClient == null) {
            return;
        }
        mqttClient.disconnect();
        dispatcher.disconnected();
    }

//    public synchronized void disconnectForcibly() throws MqttException {
//        if (mqttClient == null) {
//            return;
//        }
//        mqttClient.disconnectForcibly();
//        connectionParameters.setInUse(false);
//    }
    public synchronized boolean isConnected() {
        if (mqttClient == null) {
            return false;
        }
        return mqttClient.getState().isConnectedOrReconnect();
    }

    class Publisher implements Runnable {

        private final BlockingDeque<PublishRequest> publishingQueue;

        public Publisher() {
            this.publishingQueue = new LinkedBlockingDeque<>();
        }

        public void readyToPublish(MQTTMessageManager callback, String topic) {
            PublishRequest publishRequest = new PublishRequest(callback, topic);
            synchronized (publishingQueue) {
                if (this.publishingQueue.contains(publishRequest)) {
                    return;
                }
                this.publishingQueue.add(publishRequest);
            }
            return;
        }

        @Override
        public void run() {
            while (!timeToQuit) {
                try {

                    PublishRequest publishRequest = null;
                    publishRequest = publishingQueue.take();

                    MQTTMessage message = publishRequest.getMqttMessage();

                    while (message != null) {
                        synchronized (MQTTCommunication.this) {
                            while (!MQTTCommunication.this.isConnected()) {
                                if (timeToQuit) {
                                    return;
                                }
                                MQTTCommunication.this.wait(1000);
                            }
                        }
                        publish(publishRequest.topic, message);
                        message = null;

                    }
                } catch (InterruptedException ex) {
                    if (timeToQuit) {
                        return;
                    } else {
                        Logger.getLogger(MQTTCommunication.class.getName()).log(Level.SEVERE, "Publisher", ex);
                    }
                }
            }
        }
    }

    class PublishRequest {

        public final String topic;
        public final MQTTMessageManager publisherCallback;

        public PublishRequest(MQTTMessageManager publisherCallback, String topic) {
            this.topic = topic;
            this.publisherCallback = publisherCallback;
        }

        public MQTTMessage getMqttMessage() {
            return this.publisherCallback.getMqttMessageFor(topic);
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 59 * hash + Objects.hashCode(this.topic);
            hash = 59 * hash + Objects.hashCode(this.publisherCallback);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final PublishRequest other = (PublishRequest) obj;
            if (!Objects.equals(this.topic, other.topic)) {
                return false;
            }
            if (!Objects.equals(this.publisherCallback, other.publisherCallback)) {
                return false;
            }
            return true;
        }

        @Override
        public String toString() {
            return "PublishRequest{" + "publisherCallback=" + publisherCallback + ", topic=" + topic + '}';
        }
    }
}
