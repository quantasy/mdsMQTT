/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.mdsmqtt.communication.mqtt.v3;

/**
 *
 * @author reto
 */
public class MQTTMessage {
  public final int qos;
  public final boolean isRetained;
  public final byte[] message;

    public MQTTMessage(){
        this(0,false,null);
    }
    public MQTTMessage(int qos, boolean isRetained, byte[] message) {
        this.message=message;
        this.qos = qos;
        this.isRetained = isRetained;
    }
  
}
