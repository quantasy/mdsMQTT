/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.quantasy.mdsmqtt.communication.mqtt.v3;

import com.hivemq.client.mqtt.mqtt3.message.publish.Mqtt3Publish;

/**
 *
 * @author reto
 */
public interface MessageDispatcher {
    public void messageArrived(Mqtt3Publish publish);
    public void disconnected();
    public void connected();
    public void connectionFailed();
    public void readyToConnect();
}
